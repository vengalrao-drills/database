create database work;
show databases;

use work;

-- In main table i have brach id , branch address both will be having same data so adding only one column to the table
-- branchId is the primary key. it must be unique and not null .
create table branchInfo (branchId int primary key, branchAddr varchar (255));

-- here it has BranchId foreign key it is linked to the branchInfo. then we can get details of the branch address
create table branch (
  BranchId int
  , ISBN int
  , Title varchar(100)
  , Author varchar(100)
  , Publisher varchar(100)
  , NumOfCopies int
  , constraint branch_Id foreign key (BranchId) references branchInfo(branchId)
);