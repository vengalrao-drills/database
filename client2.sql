create table clientDetails (
  clientId int primary key
  , Name varchar(100)
  , location varchar(100)
);
-- client details --- primary key unique values
create table manangerDetails (
  manangerId int primary key
  , Name varchar(100)
  , location varchar(100)
);
-- manager details --- primary key unique values

create table staffDetails (
  staffId int primary key
  , Name varchar(100)
  , location varchar(100)
);
-- staff details --- primary key unique values



-- client , manager , staff details are used in the  main table and linked each other.
create table mainDetailsClient(
  clientId int
  , managerId int
  , contractId int
  , estimatedCost int
  , completionDate date
  , staffId int
  , constraint client_details foreign key (clientId) references clientDetails(clientId)
  , constraint manager_details foreign key (managerId) references manangerDetails(manangerId)
  , constraint staff_details foreign key (staffId) references staffDetails(staffId)
);