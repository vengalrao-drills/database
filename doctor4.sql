create table doctor (
  doctorid int primary key
  , doctorname varchar(255) not null
  , secretary varchar(255) not null
)
-- doctor table -- doctor id is linked with each other

 create table patient (
  patientid int primary key
  , patientname varchar(255) not null
  , patientdob date not null
  , patientaddress varchar(255) not null
) 
-- patient id -- is the primary key that is unique , not null values.

create table prescription (
  prescriptionid int primary key
  , drug varchar(255) not null
  , date date not null
  , dosage varchar(255) not null
) 
-- prescription id -- is the primary key that is unique , not null values.

create table doctor_patient (
  doctorid int
  , patientid int
  , primary key(doctorid, patientid)
  , foreign key (doctorid) references doctor(doctorid)
  , foreign key (patientid) references patient(patientid)
) 
-- all docor, patient has been linked.

create table doctorMain (
  patientid int
  , prescriptionid int
  , primary key (patientid, prescriptionid)
  , foreign key (patientid) references patient(patientid)
  , foreign key (prescriptionid) references prescription(prescriptionid)
)
-- main fucntion patient prescription is linked.