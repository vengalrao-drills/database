create table patientInfo (
  patientId int
  , name varchar(100)
  , dob date
  , address varchar(100)
);
alter table patientInfo
add constraint patient_id primary key (patientId);

-- added patient id as primary key - patient can only register only once

create table description (
  descriptionId int
  , drug varchar(100)
  , data date
  , dosage_in_ml int
);
alter table description
add constraint description_id primary key (descriptionId);

-- for every different descriptionId it haas different medicines. i.e for every patient. deffierent dosage, date. 
-- descriptionId is primary key


-- linking with the patientId and descriptionId complletes the table. it has corresponding columns.
CREATE TABLE patient (
  patientId INT
  , descriptionId INT
  , doctor VARCHAR(100)
  , secretary VARCHAR(100)
  , CONSTRAINT patient_Id FOREIGN KEY (patientId) REFERENCES patientInfo(patientId)
  , CONSTRAINT description_Id FOREIGN KEY (descriptionId) REFERENCES description(descriptionId)
);

